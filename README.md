# Approve Guest Service

This repository includes related technologies to deploy a mySQL container and a REST API container separately as a back-end for Approve Guest module

## Requirement
- JDK 8
- Gradle (v5.0 or greater)
- Docker Desktop

## Build
- Run `gradle clean build` to get all the dependencies installed and built

## Development server

Run `gradle bootrun` for a dev server. Navigate to `http://localhost:8080/` with listed set of endpoints to interact with the REST API (see [REST API design](https://gitlab.com/LibreFoodPantry/modules/visitmodule-tp/approveguest/ApproveGuestService/-/wikis/REST-API-design))

## Running unit tests

Run `gradle clean test` to execute the Junit tests

## Deploying docker containers
- See [mySQL with Docker](https://gitlab.com/LibreFoodPantry/modules/visitmodule-tp/approveguest/ApproveGuestService/-/wikis/mySQL-with-Docker) for instruction to deploy mySQL database Docker container
- Create a database schema and a table (see [Database schema](https://gitlab.com/LibreFoodPantry/modules/visitmodule-tp/approveguest/ApproveGuestService/-/wikis/Database-Schema))
- To deploy a REST API docker container:
  + Clone the repository and `cd` into the project root directory
  + Create a `config.json` file based on `config_sample.json` file with slight change:
    + **dbname**: database schema name from the previous step
    + **sqllogin**: can be root, but for security reason, please create another user within mySQL container
    + **sqlpw**: password to access for input user
    + **sqlserverip**: since the concept of `localhost` is different in the container, this field should be aquired using `docker inspect #MYSQL_CONTAINER_NAME#` down to the field `["NetworkSettings"]["Networks"]["bridge"]["IPAddress"]`
  + Run `docker build -t approve-guest-service:prod .` to build the container
  + Run `docker container run --name #REST_API_CONTAINER_NAME# -p 8080:8080 -t approve-guest-service:prod` to deploy the container using built image. 
  + If the container is deployed and stopped, use `docker start #REST_API_CONTAINER_NAME#` to boot it up instead

## Additional note
`gradlew.bat` or `gradlew` can be used if the machine doesn't have Gradle installed

## Documentation

- [Browse on librefoodpantry.org](https://librefoodpantry.org/#/projects/StandardProject/)
- [Edit on GitLab](docs) (in `docs/` directory)


---
Copyright &copy; 2019 The LibreFoodPantry Community. This work is licensed
under the Creative Commons Attribution-ShareAlike 4.0 International License.
To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/4.0/.
