package com.alpha.approveguestservice;
/*
	Copyright (C) 2018 Karl R. Wurst
	
	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.
	
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.
	
	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

import com.alpha.approveguestservice.Database;
import com.alpha.approveguestservice.Guest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.sql.*;

/**
 * Controller for REST API endpoints for Customers
 * 
 * @author Karl R. Wurst
 * @version Fall 2018
 */
@RestController
public class DatabaseController {
    /**
     * returns weight if the id is found in the data base
     *
     * @param id is id of the student to be checked if they are approved
     * @return weight if the id is found in the data base Http:OK or Http:404 if not in the database
     */
    @GetMapping("/isApproved/{id}")
    public ResponseEntity<Object> isGuestApproved(@PathVariable int id) throws SQLException, ClassNotFoundException {
    	ArrayList<Guest> temp = Database.select_from_id(id);
        if(temp.size() == 1 ){//id found in database
    	    return new ResponseEntity<>(temp.get(0).getWeight(), HttpStatus.OK);//returns weight
        }
    	else {//id not found in database
            return new ResponseEntity<>("Guest does not exist", HttpStatus.NOT_FOUND);//returns 404
        }
    }

    /**
     *returns the info of the guest in a  toString form if the id exists in the database
     *
     * @param id is id of the student to be searched for
     * @return guestinfo toString if id exists in the database
     */
    @GetMapping("/getGuestInfo/{id}")
    public ResponseEntity<Object> getGuestInfo(@PathVariable int id) throws SQLException{
        ArrayList<Guest> temp = Database.select_from_id(id);
        if(temp.size() == 1 ){//id found in database
            return new ResponseEntity<>(temp.get(0).toString(), HttpStatus.OK);
        }
        else {
            return new ResponseEntity<>("Guest does not exist", HttpStatus.NOT_FOUND);//returns 404
        }
    }

    /**
     * returns all objects in approval database
     *
     * @return all guests in the database, if any exist
     * @throws SQLException
    */
    @GetMapping("/Approve/all")
    public ResponseEntity<Object> approveAll() throws SQLException {
        ArrayList<Guest> temp = Database.select_from();//makes an arraylist from the database
        if (temp.isEmpty() == true) {//if arraylist is empty then the database is empty
            return new ResponseEntity<>("No Guests exist", HttpStatus.NOT_FOUND);//returns 404, nothing found in database
        }
        else {
            return new ResponseEntity<>(temp, HttpStatus.OK);//all guests in the database
        }
    }

    @GetMapping("/needsApprove/between")
    public ResponseEntity<Object> approveBetween(@RequestParam Date from, @RequestParam Date to) throws SQLException {
        ArrayList<Guest> temp = Database.select_from_between(from, to);//makes an arraylist from the database
        if (temp.isEmpty() == true) {//if arraylist is empty then the database is empty
            return new ResponseEntity<>("No Guests exist", HttpStatus.NOT_FOUND);//returns 404, nothing found in database
        }
        else {
            return new ResponseEntity<>(temp, HttpStatus.OK);//all guests in the database
        }
    }

    /**
     *adds data to our database to be used at a later date or updated
     * checks if the guest already exists in the database
     *
     * @param guest
     * @return schoolID
     * @throws SQLException
     */
    @CrossOrigin()
    @PostMapping("/approveUpdate/")
    public ResponseEntity<Object> approveUpdate(@RequestBody Guest guest) throws SQLException {
        ArrayList<Guest> temp = Database.select_from();
        int i = 0;
        while (temp.get(i) != null){
            if (temp.get(i).getSchoolID().equals(guest.getSchoolID()) == true){
                Database.update_into_weight(guest.getWeight(),guest);
                Database.update_into_approval(guest.getDateOfApproval(),guest);
                return new ResponseEntity<>(guest.getSchoolID(), HttpStatus.OK);
            }
            i++;
        }
        Database.insert_into(guest);
        return new ResponseEntity<>(guest.getSchoolID(), HttpStatus.CREATED);
    }

}