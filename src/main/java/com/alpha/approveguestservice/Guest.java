package com.alpha.approveguestservice;
/*
	Copyright (C) 2018 Karl R. Wurst
	
	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.
	
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.
	
	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

import java.sql.Date;

/**
 *
 * @author Karl R. Wurst
 * @version Fall 2018
 */
public class Guest {
	private String SchoolID;
	private int weight;
	private Date dateOfApproval;
	/**
	 * Represents a guest
	 */
	public Guest(String SchoolID,
				 Date dateOfApproval,
				 int weight) {
		this.SchoolID = SchoolID;
		this.dateOfApproval = dateOfApproval;
		this.weight = weight;
	}

	@Override
	public String toString() {
        return "Guest[SchoolID=" + SchoolID + ", Date of Approval=" + dateOfApproval+ ", Weight= " + weight + "]";
	}

	/**
	 * Customers are equal *only* if their customer numbers are equal
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Guest other = (Guest) obj;
        if (SchoolID != other.SchoolID || dateOfApproval != other.dateOfApproval || weight != other.weight)
            return false;
        return true;
    }

	public String getSchoolID() {
		return SchoolID;
	}

	public void setSchoolID(String schoolID) {
		SchoolID = schoolID;
	}

	public Date getDateOfApproval() {
		return dateOfApproval;
	}

	public void setDateOfApproval(Date dateOfApproval) {
		this.dateOfApproval = dateOfApproval;
	}

	public int getWeight() {
		return weight;
	}

	public void setWeight(int weight) {
		this.weight = weight;
	}
}