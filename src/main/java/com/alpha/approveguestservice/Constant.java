package com.alpha.approveguestservice;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileReader;
import java.io.IOException;

public class Constant {
    private static Constant instance = null;
    private String dbname;
    private String sqllogin;
    private String sqlpw;
    private String sqlserverip;

    private Constant () throws IOException, ParseException {
        parseJsonData(System.getProperty("user.dir") + "/config.json");
    }

    public static Constant getInstance() throws IOException, ParseException {
        if (instance == null) {
            instance = new Constant();
        }

        return instance;
    }

    private void parseJsonData(String path) throws IOException, ParseException {
        JSONParser parser = new JSONParser();
        Object obj = parser.parse(new FileReader(path));
        JSONObject jsonObject = (JSONObject) obj;

        setDbname(jsonObject.get("dbname").toString());
        setSqllogin(jsonObject.get("sqllogin").toString());
        setSqlpw(jsonObject.get("sqlpw").toString());
        setSqlserverip(jsonObject.get("sqlserverip").toString());
    }

    public String getDbname() {
        return dbname;
    }

    private void setDbname(String dbname) {
        this.dbname = dbname;
    }

    public String getSqllogin() {
        return sqllogin;
    }

    private void setSqllogin(String sqllogin) {
        this.sqllogin = sqllogin;
    }

    public String getSqlpw() {
        return sqlpw;
    }

    private void setSqlpw(String sqlpw) {
        this.sqlpw = sqlpw;
    }

    public String getSqlserverip() {
        return sqlserverip;
    }

    private void setSqlserverip(String sqlserverip) {
        this.sqlserverip = sqlserverip;
    }
}
