package com.alpha.approveguestservice;
/*
	Copyright (C) 2018 Karl R. Wurst
	
	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.
	
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.
	
	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

import org.json.simple.parser.ParseException;

import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;

/**
 * Provides an in-memory database for this example
 * 
 * @author Karl R. Wurst
 * @version Fall 2018
 */
public class Database {

    private static Connection conn;

    public static void connect() throws ClassNotFoundException, SQLException, IOException, ParseException {
        Class.forName("com.mysql.cj.jdbc.Driver");
        conn= DriverManager.getConnection(
                "jdbc:mysql://" + Constant.getInstance().getSqlserverip() + ":3306/" + Constant.getInstance().getDbname(), Constant.getInstance().getSqllogin(), Constant.getInstance().getSqlpw());
    }

    public static ArrayList<Guest> select_from_between(Date from_date, Date to_date) throws SQLException {
        ArrayList<Guest> result = new ArrayList<>();

        String sql = "SELECT * FROM guestinfo WHERE dateOfApproval BETWEEN ? AND ?";
        PreparedStatement stmt = conn.prepareStatement(sql);
        stmt.setDate(1, from_date);
        stmt.setDate(2, to_date);

        stmt.execute();
        ResultSet rs = stmt.getResultSet();

        while(rs.next()) {
            Guest temp = new Guest(null, null, 0);
            temp.setSchoolID(rs.getString("SchoolID"));
            temp.setDateOfApproval(java.sql.Date.valueOf(rs.getDate("dateOfApproval").toLocalDate().plusDays(1)));
            temp.setWeight(rs.getInt("weight"));
            result.add(temp);
        }

        return result;
    }

    public static ArrayList<Guest> select_from() throws SQLException {
        ArrayList<Guest> result = new ArrayList<>();

        String sql = "SELECT * FROM guestinfo";
        PreparedStatement stmt = conn.prepareStatement(sql);

        stmt.execute();
        ResultSet rs = stmt.getResultSet();

        while(rs.next()) {
            Guest temp = new Guest(null, null, 0);
            temp.setSchoolID(rs.getString("SchoolID"));
            temp.setDateOfApproval(java.sql.Date.valueOf(rs.getDate("dateOfApproval").toLocalDate().plusDays(1)));
            temp.setWeight(rs.getInt("weight"));
            result.add(temp);
        }

        return result;
    }
    public static ArrayList<Guest> select_from_id(int SchoolID) throws SQLException {
        ArrayList<Guest> result = new ArrayList<>();

        String sql = "SELECT * FROM guestinfo WHERE SchoolID = ?";
        PreparedStatement stmt = conn.prepareStatement(sql);
        stmt.setInt(1, SchoolID);

        stmt.execute();
        ResultSet rs = stmt.getResultSet();

        while(rs.next()) {
            Guest temp = new Guest(null, null, 0);
            temp.setSchoolID(rs.getString("SchoolID"));
            temp.setDateOfApproval(java.sql.Date.valueOf(rs.getDate("dateOfApproval").toLocalDate().plusDays(1)));
            temp.setWeight(rs.getInt("weight"));
            result.add(temp);
        }

        return result;
    }

    public static void insert_into(Guest guest) throws SQLException {
        String sql = "INSERT INTO guestinfo (SchoolID, dateOfApproval, weight) VALUES (? , ?, ?)";
        PreparedStatement stmt = conn.prepareStatement(sql);
        stmt.setString(1, guest.getSchoolID());
        stmt.setDate(2, java.sql.Date.valueOf(guest.getDateOfApproval().toLocalDate().plusDays(1)));
        stmt.setInt(3, guest.getWeight());

        stmt.execute();
    }

    public static void update_into_id(String id_to_be_changed, Guest guest_values) throws SQLException {
        String sql = "UPDATE guestinfo SET dateOfApproval = ?, weight = ? WHERE id = ?";
        PreparedStatement stmt = conn.prepareStatement(sql);
        stmt.setDate(1, java.sql.Date.valueOf(guest_values.getDateOfApproval().toLocalDate().plusDays(1)));
        stmt.setInt(2, guest_values.getWeight());
        stmt.setString(3, id_to_be_changed);

        stmt.execute();
    }
    public static void update_into_weight(int newWeight, Guest guest_values) throws SQLException {
        String sql = "UPDATE guestinfo SET dateOfApproval = ?, weight = ? WHERE id = ?";
        PreparedStatement stmt = conn.prepareStatement(sql);
        stmt.setDate(1, java.sql.Date.valueOf(guest_values.getDateOfApproval().toLocalDate().plusDays(1)));
        stmt.setInt(2, newWeight);
        stmt.setString(3, guest_values.getSchoolID());

        stmt.execute();
    }

    public static void update_into_approval(Date newApproval, Guest guest_values) throws SQLException {
        String sql = "UPDATE guestinfo SET dateOfApproval = ?, weight = ? WHERE id = ?";
        PreparedStatement stmt = conn.prepareStatement(sql);
        stmt.setDate(1, java.sql.Date.valueOf(newApproval.toLocalDate().plusDays(1)));
        stmt.setInt(2, guest_values.getWeight());
        stmt.setString(3, guest_values.getSchoolID());

        stmt.execute();
    }

    public static void disconnect() throws SQLException {
        conn.close();
    }
}
