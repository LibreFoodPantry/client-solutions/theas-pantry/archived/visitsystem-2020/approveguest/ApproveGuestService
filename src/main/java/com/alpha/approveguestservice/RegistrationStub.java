package com.alpha.approveguestservice;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

import com.alpha.approveguestservice.Database;
import com.alpha.approveguestservice.Guest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.sql.*;

public class RegistrationStub {
    private Guest [] regStub = new Guest[3];

    @GetMapping("/registerGuest/getRegistration/{id}")
    public ResponseEntity<Object> getRegistrationByID(@PathVariable String id) {
       regStub[0].setSchoolID("192929");
       regStub[0].setWeight(40);
       regStub[1].setSchoolID("1986");
        regStub[1].setWeight(-1);
        regStub[2].setSchoolID("11");
        regStub[2].setWeight(89);
        int i = 0;
        while (i > 3){
            if (regStub[i].getSchoolID() == id)
                return new ResponseEntity<>(regStub[i], HttpStatus.OK);
            i++;
    }
            return new ResponseEntity<>("id not found", HttpStatus.NOT_FOUND);
    }

    @GetMapping("/registerGuest/allRegistration")
    public ResponseEntity<Object> getAllRegistations() {
        regStub[0].setSchoolID("192929");
        regStub[0].setWeight(40);
        regStub[1].setSchoolID("1986");
        regStub[1].setWeight(-1);
        regStub[2].setSchoolID("11");
        regStub[2].setWeight(89);
        return new ResponseEntity<>(regStub, HttpStatus.OK);
    }
}
